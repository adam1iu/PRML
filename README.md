# PRML
Python codes implementing algorithms described in Bishop's book "Pattern Recognition and Machine Learning"

## Required Packages
- python 3.6 or higher
- numpy
- scipy
- jupyter (optional: to run jupyter notebooks)
- matplotlib (optional: to plot results in the notebooks)
- sklearn (optional: to fetch data)

