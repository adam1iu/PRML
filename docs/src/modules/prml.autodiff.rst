prml.autodiff package
=====================

.. automodule:: prml.autodiff
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   prml.autodiff.linalg
   prml.autodiff.optimizer
   prml.autodiff.random
   prml.autodiff.signal
