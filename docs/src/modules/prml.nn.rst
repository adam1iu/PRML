prml.nn package
===============

.. automodule:: prml.nn
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   prml.nn.functions
   prml.nn.initializers
   prml.nn.io
   prml.nn.layers
   prml.nn.normalization
