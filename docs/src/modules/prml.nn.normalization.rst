prml.nn.normalization package
=============================

.. automodule:: prml.nn.normalization
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

prml.nn.normalization.batch\_normalization module
-------------------------------------------------

.. automodule:: prml.nn.normalization.batch_normalization
   :members:
   :undoc-members:
   :show-inheritance:

