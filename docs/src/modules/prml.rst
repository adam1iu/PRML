prml package
============

.. automodule:: prml
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   prml.autodiff
   prml.bayesnet
   prml.clustering
   prml.dimreduction
   prml.kernel
   prml.linear
   prml.markov
   prml.nn
   prml.preprocess
   prml.rv
   prml.sampling
