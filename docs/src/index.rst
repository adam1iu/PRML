.. PRML documentation master file, created by
   sphinx-quickstart on Sat Jun 29 06:56:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PRML's documentation!
================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   modules/*



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
